# Project: pizzeria-corleone

by: Iceyisaak

### This project features:

- VueJS project
- CRUD for Restaurant Menu
- Firebase connection

### Technologies used
- VueJS
- FontAwesome
- SCSS
- Bootstrap

## Installation
1. `npm install` to create node_modules
2. `npm run dev` to run the project
3. ENJOY!
