import firebase from 'firebase'

const FirebaseConfig = {
    apiKey: "AIzaSyCYz9lFPnJNvHdhy3NlUOjZ6aIs_bRy2VE",
    authDomain: "pizza-plannet.firebaseapp.com",
    databaseURL: "https://pizza-plannet.firebaseio.com",
    projectId: "pizza-plannet",
    storageBucket: "pizza-plannet.appspot.com",
    messagingSenderId: "783574387197",
    appId: "1:783574387197:web:00d251e062fd87aa7f3194",
    measurementId: "G-0R431CP3EH"
  };

  const firebaseApp = firebase.initializeApp(FirebaseConfig);
  const db = firebaseApp.database()
  export const dbMenuRef = db.ref('menu');
  export const dbOrdersRef = db.ref('orders');