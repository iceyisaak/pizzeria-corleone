import Vue from 'vue'
import VueRouter from 'vue-router'
import { routes } from './routes'
import { store } from './store/store.js'
import Accounting from 'accounting-js'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPizzaSlice } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import App from './App.vue'

Vue.use(VueRouter)

library.add(faPizzaSlice)

Vue.component('font-awesome-icon', FontAwesomeIcon)

const router = new VueRouter({
  routes,
  mode: 'history'
})

// router.beforeEach((to, from, next) => {
//   console.log(to)
// })

Vue.filter('currency', function (val) {
  return Accounting.formatMoney(val, {
    symbol: "€",
    thousand: ".",
    decimal: ",",
  })
})

// Vue.component('global-component', () => import('./components/Menu.vue'))

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
